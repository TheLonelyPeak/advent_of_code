"""Advent of code 2022 day 1 - https://adventofcode.com/2022/day/1

The objective of the second part is to find the top three elves carrying
the most Calories and the sum Calories being carried"""

def find_top3_elves(input_list):
    """Takes in a list of calories and calculates the total amount of calories of 
    each elf and compares it. It then returns the the total sum of calories of
    the top three elves"""
    cal_sum = 0
    max_cal = 0
    calorie_sum_list = []

    for indx, calorie in enumerate(input_list):
        if input_list[indx] != "": #checks if the element is not a newline/empty
            cal_sum += int(calorie)
        else:
            calorie_sum_list.append(cal_sum)
            cal_sum = 0

        if cal_sum > max_cal:
            max_cal = cal_sum
    calorie_sum_list.append(cal_sum)
    calorie_sum_list.sort(reverse=True)
    return sum(calorie_sum_list[:3])

if __name__ == '__main__': # pragma: no cover
    with open("2022/day1/input") as file:
        data = file.read()
        data = data.splitlines()
    print(find_top3_elves(data))
