"""Tests for day1 part1"""
import part1

class TestClass:
    ex_input = """1000
    2000
    3000

    4000

    5000
    6000

    7000
    8000
    9000

    10000"""

    def test_calculate_calories(self):
        """Checks if the highest amount of calories in the input is equal to 24000"""
        assert part1.calculate_calories(self.ex_input.splitlines()) == 24000
