"""Tests for day1 part1"""
import part2

class TestClass:
    ex_input = """1000
    2000
    3000

    4000

    5000
    6000

    7000
    8000
    9000

    10000"""

    def test_top3_elves(self):
        """Checks if the sum of calories of the top three elves in the input is equal to 45000"""
        assert part2.find_top3_elves(self.ex_input.splitlines()) == 45000
