"""Advent of code 2022 day 1 - https://adventofcode.com/2022/day/1

The objective of the first part is to find the Elf carrying
the most Calories and the total Calories being carried"""


def calculate_calories(input_list):
    """Takes in a list of calories and calculates the total amount of calories of 
    each elf and compares it. It then returns the highest calories calculated"""

    cal_sum = 0
    max_cal = 0

    for indx, calorie in enumerate(input_list):
        if input_list[indx] != "": #checks if the element is not a newline/empty
            cal_sum += int(calorie)
        else:
            cal_sum = 0

        if cal_sum > max_cal:
            max_cal = cal_sum

    return max_cal

if __name__ == '__main__': # pragma: no cover
    with open("2022/day1/input") as file:
        data = file.read()
        data = data.splitlines()
    print(calculate_calories(data))
